from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView

from rest_framework import routers

from users.views import UserViewSet, UserAPI
from teachers.views import SubjectViewSet, SubjectAssignmentViewSet
from students.views import StudentClassViewSet


router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'subjects', SubjectViewSet)
router.register(r'subject-assignments', SubjectAssignmentViewSet)
router.register(r'student-classes', StudentClassViewSet)


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/v1/', include(router.urls)),
    url(r'^api/v1/user/signin/', UserAPI.as_view({'post': 'signin'})),
    url(r'^api/v1/user/signup/', UserAPI.as_view({'post': 'signup'})),
    url(r'^api/v1/user/signout/', UserAPI.as_view({'post': 'signout'})),
    url(r'^api/v1/user/current/', UserAPI.as_view({'get': 'get_current_user'})),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += [
    url(r'^', TemplateView.as_view(template_name="base.html"), name="base"),
]
