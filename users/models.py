from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin

from .managers import CustomUserManager

TEACHER = 'teacher'
STUDENT = 'student'
ADMIN = 'admin'

LIST_OF_ROLES = (
    (TEACHER, 'Teacher'),
    (STUDENT, 'Student'),
    (ADMIN, 'Admin')
)

class User(AbstractBaseUser, PermissionsMixin):
    """ Create a custom class for the users or accounts to enable adding of
    custom fields """

    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=50, null=True)
    last_name = models.CharField(max_length=50, null=True)
    role = models.CharField(max_length=7, choices=LIST_OF_ROLES, default=STUDENT)

    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ('first_name', 'last_name')

    objects = CustomUserManager()

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def get_short_name(self):
        return self.first_name

# Steps
# 1. Import `AbstractBaseUser` which has the core implementation for a user model
# 2. Define your model fields. Add `get_short_name()` method.
# 3. Use `USERNAME_FIELD` to identify your unique field which will replace `username`
#    as the default unique field
# 4. Create a custom manager for a custom user model
# 5. Inherit `PermissionsMixin` for the permissions
