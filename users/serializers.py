from rest_framework import serializers
from django.contrib.auth import get_user_model, authenticate

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()
        fields = ('id', 'email', 'first_name', 'last_name', 'password', 'role')
        extra_kwargs = {
            'password': {'write_only': True}
        }


class SigninSerializer(serializers.Serializer):

    email = serializers.EmailField()
    password = serializers.CharField(write_only=True)

    user_cache = None
    signin_error = 'Email or password is incorrect'

    def validate(self, data):
        email = data.get('email')
        password = data.get('password')

        if not (email or password):
            raise serializers.ValidationError(self.signin_error)

        self.user_cache = authenticate(email=email, password=password)
        if not self.user_cache or not self.user_cache.is_active:
            raise serializers.ValidationError(self.signin_error)

        return data