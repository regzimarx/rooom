from django.shortcuts import render
from django.contrib.auth import get_user_model, login, logout
from django.forms import model_to_dict

from rest_framework import viewsets
from rest_framework.response import Response

from .serializers import (
    UserSerializer,
    SigninSerializer
)
from users.models import User


class UserAPI(viewsets.ViewSet):

    serializer_class = UserSerializer
    
    def signup(self, *args, **kwargs):
        serializer = UserSerializer(data=self.request.data)
        if serializer.is_valid():

            User = get_user_model()
            instance = User.objects.create_user(
                email=self.request.data.get('email'),
                first_name=self.request.data.get('first_name'),
                last_name=self.request.data.get('last_name'),
                password=self.request.data.get('password'),
                role=self.request.data.get('role')
            )

            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def signout(self, *args, **kwargs):
        logout(self.request)
        return Response(status=200)

    def signin(self, *args, **kwargs):
        serializer = SigninSerializer(data=self.request.data)

        if serializer.is_valid():
            login(self.request, serializer.user_cache)
            serializer = UserSerializer(serializer.user_cache)
            return Response(serializer.data, status=200)
        return Response(serializer.errors, status=400)

    def get_current_user(self,  *args, **kwargs):
        serializer = UserSerializer(model_to_dict(self.request.user))
        return Response(serializer.data, status=200)


class UserViewSet(viewsets.ModelViewSet):
    """ API endpoint for users """

    queryset = User.objects.all()
    serializer_class = UserSerializer
