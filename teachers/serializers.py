from rest_framework import serializers
from .models import Subject, SubjectAssignment
from users.models import User
from users.serializers import UserSerializer
from students.serializers import StudentClassSerializer
from students.models import StudentClass

class SubjectSerializer(serializers.ModelSerializer):
    """ Serializer for subejcts """

    class Meta:
        model = Subject
        fields = '__all__'
        depth = 1


class SubjectAssignmentSerializer(serializers.ModelSerializer):
    """ Serializer for subject assignments """


    class Meta:
        model = SubjectAssignment
        fields = '__all__'
        read_only_fields = ('id', 'subject', 'teacher', 'student_class', )
        depth = 2