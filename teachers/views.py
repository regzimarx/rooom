from django.shortcuts import render

from rest_framework import viewsets
from rest_framework.response import Response

from .models import Subject, SubjectAssignment
from students.models import StudentClass
from .serializers import SubjectSerializer, SubjectAssignmentSerializer


class SubjectViewSet(viewsets.ModelViewSet):
    """ API endpoint for subjects """

    queryset = Subject.objects.all()
    serializer_class = SubjectSerializer


class SubjectAssignmentViewSet(viewsets.ModelViewSet):
    """ API endpoint for subject assignments """

    queryset = SubjectAssignment.objects.all()
    serializer_class = SubjectAssignmentSerializer

    def create(self, request):
        serializer = SubjectAssignmentSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save(
                subject=Subject.objects.get(pk=request.data.get('subject')),
                teacher=request.user,
                student_class=StudentClass.objects.get(pk=request.data.get('student_class'))
            )
            return Response(serializer.data, status=200)
        return Response(serializer.errors, status=400)
