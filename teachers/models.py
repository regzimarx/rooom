from django.db import models

class Subject(models.Model):
    """ Model for subject """

    name = models.CharField(max_length=250)
    description = models.CharField(max_length=250)

    def __str__(self):
        return self.name


class SubjectAssignment(models.Model):
    """ Model for assignment of subject, teacher and class """

    subject = models.ForeignKey('teachers.Subject')
    teacher = models.ForeignKey('users.User')
    student_class = models.ForeignKey('students.StudentClass')

    def __str__(self):
        return '{} - {} - {}'.format(self.subject, self.teacher, self.student_class)


class Lesson(models.Model):
    """ Model for lessons """

    subject_assignment = models.ManyToManyField('teachers.SubjectAssignment')
    title = models.CharField(max_length=250)
    content = models.TextField()
    files = models.ManyToManyField('teachers.FileCollection', blank=True)

    def __str__(self):
        return "{} - {}".format(self.title, self.subject_assignment)


class FileCollection(models.Model):
    """ Model for files """
    name = models.CharField(max_length=250)
    uploaded_file = models.FileField(upload_to='uploads/')

    def __str__(self):
        return self.name
