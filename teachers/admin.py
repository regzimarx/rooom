from django.contrib import admin

from .models import Subject, SubjectAssignment, Lesson, FileCollection

admin.site.register(Subject)
admin.site.register(SubjectAssignment)
admin.site.register(Lesson)
admin.site.register(FileCollection)
