from django.db import models

class StudentClass(models.Model):
    """docstring for StudentClass"""

    GRADE7 = '7'
    GRADE8 = '8'
    GRADE9 = '9'
    GRADE10 = '10'
    GRADE11 = '11'
    GRADE12 = '12'

    LIST_OF_GRADES = (
        (GRADE7, 'Grade VII'),
        (GRADE8, 'Grade VIII'),
        (GRADE9, 'Grade IX'),
        (GRADE10, 'Grade X'),
        (GRADE11, 'Grade XI'),
        (GRADE12, 'Grade XII'),
    )

    grade = models.CharField(choices=LIST_OF_GRADES,max_length=20)
    section = models.CharField(max_length=100)
    students = models.ManyToManyField('users.User', blank=True)

    class Meta:

        verbose_name_plural = 'Student classes'
        
    def __str__(self):
        return '{} {}'.format(self.grade, self.section)