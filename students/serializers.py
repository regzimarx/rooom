from rest_framework import serializers
from .models import StudentClass
from users.models import User

class StudentClassSerializer(serializers.ModelSerializer):
    """ Serializer for student classes """

    class Meta:
        model = StudentClass
        fields = '__all__'
        depth = 1