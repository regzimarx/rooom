from django.shortcuts import render

from rest_framework import viewsets
from rest_framework.response import Response

from .serializers import StudentClassSerializer
from .models import StudentClass

class StudentClassViewSet(viewsets.ModelViewSet):
    """ API endpoint for student classes """

    queryset = StudentClass.objects.all()
    serializer_class = StudentClassSerializer
